import { Schema, array, string, tuple } from 'yup';
import { Job, jobListSchema } from './JobStore/jobSchema';
import { TagColor, tagColorSchema } from './TagsStore';

export interface AppStorage<T> {
    updateValue(v: T): void;
    value: T;
}

export type StorageType = 'localstorage' | 'memory';

export interface StorageFactory {
    createJobStorage: () => AppStorage<Job>
    createTagStorage: () => AppStorage<Map<string, TagColor>>
}

export class LocalStorageFactory implements StorageFactory {
    createJobStorage() {
        return new LocalStorageObjectCache<Job[]>('cachedJobs', [], jobListSchema)
    }

    createTagStorage() {
        return new LocalStorageMapCache<string, TagColor>(
            'jobtags',
            new Map<string, TagColor>(),
            string().required(),
            tagColorSchema
        )
    }
}



export class LocalStorageCache<T> implements AppStorage<T> {
    private _defaultString: string;

    constructor(
        private _itemKey: string,
        defaultValue: T,
        private _serialize: (value: T) => string,
        private _deserialize: (value: string) => T
    ) {
        const value = window.localStorage.getItem(this._itemKey);
        this._defaultString = this._serialize(defaultValue);
        if (!this.value) {
            window.localStorage.setItem(this._itemKey, this._defaultString);
        }
    }

    get value(): T {
        return this._deserialize(window.localStorage.getItem(this._itemKey) || this._defaultString);
    }

    updateValue(newValue: T) {
        window.localStorage.setItem(this._itemKey, this._serialize(newValue));
    }
};

export class LocalStorageObjectCache<T> extends LocalStorageCache<T> {
    constructor(itemKey: string, defaultValue: T, schema: Schema) {
        super(
            itemKey,
            defaultValue,
            (value: T) => {
                return JSON.stringify(schema.validateSync(value))
            },
            (value: string) => {
                return schema.cast(JSON.parse(value));
            },
        )
    }
}

export class LocalStorageMapCache<T, U> extends LocalStorageCache<Map<T, U>> {
    constructor(itemKey: string, defaultValue: Map<T, U>, keySchema: Schema<T>, valueSchema: Schema<U>) {
        const entriesSchema = array().of(tuple([keySchema, valueSchema]).required())
            
        super(
            itemKey,
            defaultValue,
            (value: Map<T, U>) => {
                return JSON.stringify(entriesSchema.validateSync(value.entries()))
            },
            (value: string) => {
                const v = entriesSchema.cast(JSON.parse(value) ?? []);
                return new Map<T, U>(v);
            }
        )
    }
}

export default LocalStorageCache;
