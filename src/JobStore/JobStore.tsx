import React, { useState, useEffect, ReactChildren } from 'react';
import { reach, ValidationError } from 'yup';
import { Job, jobListSchema, jobSchema } from './jobSchema';
import { StateCache } from '../lib/cache';

export interface JobContextSchema {
    jobs: Job[];
    currentJob?: Job;
    status: string;
    addNewJob: (job: Partial<Job>) => Job["id"];
    updateJob: (id: Job["id"], value: Record<Job["id"], Partial<Job>>) => Promise<void>;
    deleteJob: (id: Job["id"]) => void,
    selectJob: (id: Job["id"]) => void,
    clearCurrentJob: () => void,
}

export interface JobProviderProps {
    cache: StateCache<Job[]>;
    children: ReactChildren;
}

const JobContext = React.createContext<JobContextSchema>({
    jobs: [],
    status: 'idle',
    addNewJob: () => -1,
    updateJob: async () => { },
    deleteJob: () => { },
    selectJob: () => { },
    clearCurrentJob: () => { },
});

const JobProvider = ({ cache, children }: JobProviderProps) => {
    const [jobs, setJobs] = useState<Job[]>(cache.value);
    const [status, setStatus] = useState('idle');
    const [currentJob, setCurrentJob] = useState();
    const [currentJobId, setCurrentJobId] = useState<Job["id"]>();

    useEffect(() => {
        jobListSchema.validateAt('[].id', [{ id: currentJobId }])
            .then(() => setCurrentJob(jobs.find(job => job.id === currentJobId)))
    }, [currentJobId, jobs]);
    
    const selectJob = (id: Job["id"]) => setCurrentJobId(id);

    const clearCurrentJob = () => selectJob(undefined);

    const addNewJob = (newJob: Partial<Job>) => {
        setStatus('adding');
        
        newJob.id = (jobs.length ? jobs[jobs.length - 1].id : 0) + 1;
        const newjobs = [...jobs, jobSchema.cast(newJob)];

        return jobListSchema.validate(newjobs)
            .then(() => {
                setStatus('idle');
                setJobs(newjobs);
                cache.updateValue(newjobs);
                return newJob.id;
            }).catch(e => {
                setStatus('idle');
                throw e;
            });
    }

    const updateJob: JobContextSchema["updateJob"] = (id, { id: _, ...job }) => {
        setStatus('updating');
        const oldJobIndex = jobs.findIndex((listJob) => id === listJob.id);

        if (oldJobIndex === -1) {
            setStatus('ide');
            return Promise.reject(new ValidationError('Job does not exist'));
        }
        
        const newjobs = [
            ...jobs.slice(0, oldJobIndex),
            { id, ...jobSchema.cast(job) },
            ...jobs.slice(oldJobIndex + 1)
        ];

        return jobListSchema.validate(newjobs)
            .then(() => {
                setStatus('idle');
                setJobs(newjobs);
                cache.updateValue(newjobs);
            }).catch(e => {
                setStatus('idle');
                throw e;
            });
    }

    const deleteJob: JobContextSchema["deleteJob"] = id => {
        const jobIndex = jobs.findIndex(job => job.id === id);
        if (jobIndex) {
            const newJobs = [...jobs.slice(0, jobIndex), ...jobs.slice(jobIndex + 1)];
            setJobs(newJobs);
            cache.updateValue(newJobs);
        }
    };

    return (
        <JobContext.Provider value={{ jobs, currentJob, selectJob, clearCurrentJob, status, addNewJob, updateJob, deleteJob }}>
            { children }
        </JobContext.Provider>
    );
};

const JobConsumer = JobContext.Consumer;

export {
    JobContext,
    JobProvider,
    JobConsumer
};
