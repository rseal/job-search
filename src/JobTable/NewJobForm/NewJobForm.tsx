import { useState } from "react";
import {
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  TextField,
} from "@material-ui/core";
import { type Job } from "../../JobStore/jobSchema";
import TagsInput from "../../TagsInput";

export interface NewJobFormProps {
  addNewJob: (job: Partial<Job>) => Promise<Job["id"]>;
  onCloseForm: () => void;
}

export function NewJobForm({ onCloseForm, addNewJob }: NewJobFormProps) {
  const [newJob, updateNewJob] = useState<Partial<Job>>({});
  const [isSaving, setIsSaving] = useState<boolean>(false);
  const [error, setError] = useState<any>();

  const onChangeJobField = (fieldName: string) => (event: any) => {
      const value = event.target.value;
    updateNewJob((prev: Job[typeof fieldName]) => ({
      ...prev,
      [fieldName]: value,
    }));
  };

  const onSaveNewJob = async (addAnother: boolean) => {
    setIsSaving(true);
    try {
      await addNewJob(newJob);
      if (addAnother) {
        updateNewJob({});
      } else {
        onCloseForm();
      }
    } catch (e) {
      setError(e);
    }
    setIsSaving(false);
  };

  return (
    <Dialog open>
      <DialogTitle>Create new Job</DialogTitle>

      <DialogContent>
        {error}
        <Grid container direction="column">
          <Grid item>
            <TextField
              id="company"
              label="Company"
              onChange={onChangeJobField("company")}
              inputProps={{
                readOnly: isSaving,
              }}
              placeholder="Gitlab"
            />
          </Grid>
          <Grid item>
            <TextField
              id="title"
              label="Job Title"
              value={newJob.title || ""}
              onChange={onChangeJobField("title")}
              placeholder="Senior Software Engineer"
              inputProps={{
                readOnly: isSaving,
              }}
            />
          </Grid>
          <Grid item>
            <TextField
              id="postedDate"
              value={newJob.postedDate || ""}
              onChange={onChangeJobField("postedDate")}
              type="date"
              placeholder="Date Posted"
              inputProps={{
                readOnly: isSaving,
              }}
              label="Date Posted"
            />
          </Grid>
          <Grid item>
            <TextField
              id="applicationLink"
              value={newJob.applicationLink || ""}
              onChange={onChangeJobField("applicationLink")}
              placeholder="https://"
              inputProps={{
                readOnly: isSaving,
              }}
              label="Application Link"
            />
          </Grid>
          <Grid item>
            <TagsInput
              value={newJob.tags || []}
              onChange={(tags) => updateNewJob({ ...newJob, tags })}
              inputProps={{
                readOnly: isSaving,
              }}
            />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button isDisabled={isSaving} onClick={() => onCloseForm()}>
          Cancel
        </Button>
        <Button isDisabled={isSaving} onClick={() => onSaveNewJob(false)}>
          Save
        </Button>
        <Button isDisabled={isSaving} onClick={() => onSaveNewJob(true)}>
          Save and Add Another
        </Button>
      </DialogActions>
    </Dialog>
  );
}
