import { Dispatch, SetStateAction, useState } from 'react';
import { AppStorage } from '../LocalStorageCache';

export function useCachedState<T>(cache: AppStorage<T>) : [T, Dispatch<SetStateAction<T>>] {
    const [state, setState] = useState(cache.value);

    const setCachedState = (newValue: T) => {
        cache.updateValue(newValue);
        setState(newValue)
    }

    const setCachedStateFn = (fn: (oldValue: T) => T) => {
        setState((oldValue: T) => {
            const newValue = fn(oldValue);
            cache.updateValue(newValue);
            return newValue;
        })
    }

    const setStateWithCacheEffect = (setter: T | ((v: T) => T)) => {
        if (typeof setter === 'function') {
            return setCachedStateFn(setter as (v: T) => T);
        }
        return setCachedState(setter);
    }

    return [state, setStateWithCacheEffect];
}