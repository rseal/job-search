import { useContext } from "react";
import { NewJobForm as NewJobFormVisual } from "./NewJobForm";
import { JobContext } from "../../JobStore";

export interface NewJobFormProps {
  onCloseForm: () => void;
}

export function NewJobForm({ onCloseForm }: NewJobFormProps) {
  const { addNewJob } = useContext(JobContext);

  return <NewJobFormVisual addNewJob={addNewJob} onCloseForm={onCloseForm} />;
}
