import React, { ReactChildren } from 'react';
import { string, InferType } from 'yup';
import { AppStorage } from '../LocalStorageCache';
import { useCachedState } from '../hooks/useCachedState';

export const tagColorSchema = string().required().oneOf(['red', 'green', 'blue', 'yellow', 'default']);

export type TagColor = InferType<typeof tagColorSchema>

export interface TagsContext {
    tags: Map<string, TagColor>;
    addTag: (tag: string) => void;
    updateTag: (tag: string, color: TagColor) => void;
    deleteTag: (tag: string) => void;
}

const TagsContext = React.createContext<TagsContext>({
    tags: new Map<string, TagColor>(),
    addTag: () => { },
    updateTag: () => { },
    deleteTag: () => { },
});

interface TagsProviderProps {
    cache: AppStorage<Map<string, TagColor>>;
    children: ReactChildren;
}

const TagsProvider = ({ cache, children }: TagsProviderProps) => {
    const [tags, setTags] = useCachedState(cache);

    const updateTag = (tag: string, color: TagColor) => {
        const newTags = new Map(tags.entries());
        newTags.set(tag, color);
        setTags(newTags); 
    }

    const addTag = (tag: string) => updateTag(tag, 'default');

    const deleteTag = (tag: string) => {
        const newTags = new Map(tags.entries());
        newTags.delete(tag);
        setTags(newTags);
    }

    return (
        <TagsContext.Provider value={{ tags, addTag, updateTag, deleteTag }}>
            {children}
        </TagsContext.Provider>
    );
};

export { TagsContext, TagsProvider };
